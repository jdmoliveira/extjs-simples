Ext.require(['Ext.grid.*', 'Ext.data.*', 'Ext.panel.*', 'Ext.layout.container.Border']);
Ext.onReady(function() {

	Ext.define('Contacto', {
		extend : 'Ext.data.Model',
		fields : ['id', 'nome', 'telefone', 'email']
	});

	var store = Ext.create('Ext.data.Store', {
		model : 'Contacto',
		autoSync : true, // atualiza na BD sempre que sofrer alterações...
		autoLoad : true,
		proxy : {
			type : 'ajax',
			api : {
				read : 'php/listaContactos.php'
			},
			reader : {
				type : 'json',
				root : 'contactos',
				successProperty : 'success'
			}
		}
	});

	var grid = Ext.create('Ext.grid.Panel', {
		store : store,
		title : 'Contactos',
		columns : [{
			text : "Id",
			width : 40,
			dataIndex : 'id'
		}, {
			text : "Nome",
			flex : 1,
			dataIndex : 'nome',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}, {
			text : "Telefone",
			width : 115,
			dataIndex : 'telefone',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}, {
			text : "Email",
			width : 120,
			dataIndex : 'email',
			sortable : true,
			field : {
				xtype : 'textfield'
			}
		}],
		viewConfig : {
			forceFit : true
		},
		region : 'center'
	});

	Ext.widget('tabpanel', {
		renderTo : 'tabela',
		width : 600,
		height : 400,
		activeTab : 0,
		defaults : {
			bodyPadding : 20
		},
		items : [{
			html : "Lista dos meus contactos.",
			title : 'Introdução',
			closable : true
		}, {
			title : 'Contactos',
			items : [grid]
		}]
	});
});
